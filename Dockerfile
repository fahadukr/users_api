FROM python:latest
COPY .  /users_api/
WORKDIR /users_api
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD [ "python", "index.py" ]