from pymongo import MongoClient
from .settings import DB_DRIVER, DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_DATABASE, DB_COLLECTION


def db_connection():
    db_dsn = DB_DRIVER + DB_USER + ':' + DB_PASSWORD + '@' + DB_HOST + ':' + DB_PORT + '/' + DB_DATABASE
    client = MongoClient(db_dsn)
    collection_cnx = client[DB_DATABASE][DB_COLLECTION]
    return collection_cnx
