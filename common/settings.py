# Property of Intetics Inc.

DB_DRIVER = 'mongodb://'
DB_USER = 'user'
DB_PASSWORD = 'password'
DB_HOST = 'host'
DB_PORT = 'port'
DB_DATABASE = 'db'
DB_COLLECTION = 'collection'
TELEGRAM_TOKEN = 'tg_token'
TELEGRAM_CHAT_ID = 'tg_chat_id'
ORIGIN_CORS = '*'
HOST = '0.0.0.0'
