from flask import request, make_response, current_app
from datetime import timedelta
from functools import update_wrapper
from telegram import Bot
from datetime import datetime


def date_formater(time):
    """
    function for formatting date time in the needed format
    for example, to change  insertion date from UTC to Kyiv time :
    import timedelta
    time_now = datetime.strftime(date + timedelta(hours=3), '%Y-%m-%d %H:%M:%S' + ' Europe/Kiev')
    """
    time_now = datetime.strftime(time, '%Y-%m-%d %H:%M:%S')
    return time_now


class TelegramBot:

    def __init__(self, token, chat_id):
        self.token = token
        self.chat_id = chat_id

    def send(self, msg):
        bot = Bot(token=self.token)
        return bot.send_message(self.chat_id, text=msg, timeout=15)


def cross_domain_cors(origin=None, methods=None, headers=None, max_age=21600,
                      attach_to_all=True, automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        """
        Determines which methods are allowed
        """
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        """
        The decorator function
        """

        def wrapped_function(*args, **kwargs):
            """
            Caries out the actual cross domain code
            """
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator
