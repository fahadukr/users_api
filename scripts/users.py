from flask import request, jsonify
from .helper import date_formater
from datetime import datetime
from pymongo.errors import DuplicateKeyError


class Users:
    def __init__(self, connection, telegram_bot):
        self.connection = connection
        self.telegram_bot = telegram_bot

    def insert_user(self):
        """
        insert users
        * email is obligatory
        """
        user_data = request.form.to_dict()
        if 'email' not in user_data.keys():
            return 'Please add an email'
        time_now = datetime.now()
        formated_time = date_formater(time_now)
        user_data.update({'creationTime': formated_time})
        try:
            self.connection.insert_one(user_data)
            user_data['_id'] = str(user_data['_id'])
            self.telegram_bot.send('Inserted User: ' + str(user_data))
            return jsonify({'Inserted Successfully': user_data})
        except DuplicateKeyError:
            self.telegram_bot.send('Insertion is not allowed: ' + user_data['email'] + ' already exists')
            return f"duplicate key error: user email {user_data['email']} already exists"

    def find_user(self, email):
        """
        find user by email
        """
        try:
            user = self.connection.find_one({"email": email})
            user['_id'] = str(user['_id'])
            self.telegram_bot.send('User Found ' + str(user))
            return jsonify(user)
        except:
            return 'The e-mail is incorrect or does not exist'

    def delete_user(self, email):
        """
        delete user by email
        """
        try:
            deleted_user = self.connection.delete_one({'email': email})
            if deleted_user.deleted_count > 0:
                self.telegram_bot.send('Deleted User: ' + str(email))
                return jsonify({'Deleted User: ': email})
            return 'The e-mail is incorrect or does not exist'
        except:
            return 'error while deleting from the database'

    def update_user(self, email):
        """
        update user by his email
        """
        fields = request.form.to_dict()
        time_now = datetime.now()
        formated_time = date_formater(time_now)
        fields.update({'updateTime': formated_time})
        try:
            updated_user = self.connection.update_one({'email': email}, {'$set': fields})
            if updated_user.modified_count > 0:
                self.telegram_bot.send('Successfully Updated User: ' + email + ', set ' + str(fields))
                return jsonify({'Updated Successfully': email})
            return 'The e-mail is incorrect or does not exist'
        except:
            return 'error while updating the database'
