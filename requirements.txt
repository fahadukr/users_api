Flask==2.0.1
pymongo==3.11.3
certifi==2020.12.5
chardet==4.0.0
python-dateutil==2.8.1
pytz==2021.1
python-telegram-bot==13.4.1