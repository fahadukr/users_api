from flask import Flask
from common.connect import db_connection
from scripts.users import Users
from common.settings import ORIGIN_CORS, HOST, TELEGRAM_TOKEN, TELEGRAM_CHAT_ID
from scripts.helper import cross_domain_cors, TelegramBot



app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False


@app.route('/')
def hello():
    return 'Property of Intetics Inc'


@app.route('/insert/user/', methods=['POST'])
@cross_domain_cors(origin=ORIGIN_CORS)
def insert_user():
    db = Users(db_connection(),TelegramBot(TELEGRAM_TOKEN, TELEGRAM_CHAT_ID))
    return db.insert_user()


@app.route('/find/user/<email>', methods=['POST'])
@cross_domain_cors(origin=ORIGIN_CORS)
def get_user(email):
    db = Users(db_connection(), TelegramBot(TELEGRAM_TOKEN, TELEGRAM_CHAT_ID))
    return db.find_user(email)


@app.route('/delete/user/<email>', methods=['POST'])
@cross_domain_cors(origin=ORIGIN_CORS)
def del_user(email):
    db = Users(db_connection(), TelegramBot(TELEGRAM_TOKEN, TELEGRAM_CHAT_ID))
    return db.delete_user(email)

@app.route('/update/user/<email>', methods=['POST'])
@cross_domain_cors(origin=ORIGIN_CORS)
def upd_user(email):
    db = Users(db_connection(), TelegramBot(TELEGRAM_TOKEN, TELEGRAM_CHAT_ID))
    return db.update_user(email)


if __name__ == '__main__':
    app.run(host=HOST)
